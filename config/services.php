<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [
        'client_id' => '501409456866-j2kfq4rd0vns4j096darkfpk83c9s1b3.apps.googleusercontent.com',
        'client_secret' => '2z9WaufFLILUt2zNLEkVWk9c',
        'redirect' => 'http://trainCommander.dev/auth/google/callback',
    ],
    'facebook' => [
        'client_id' => '1812995438924540',
        'client_secret' => '9031dccfd35a0c5a912ed3912ba98537',
        'redirect' => 'http://trainCommander.dev/',
    ],

];
