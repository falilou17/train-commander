<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('departureStation');
            $table->string('arrivalStation');
            $table->boolean('status');
            $table->bigInteger('departureTime');
            $table->bigInteger('arrivalTime');
            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('id')->on('users');
            $table->integer('paypalId')->unsigned();
            $table->foreign('paypalId')->references('id')->on('paypals');
            $table->integer('tripId')->unsigned();
            $table->foreign('tripId')->references('id')->on('partial_trips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
