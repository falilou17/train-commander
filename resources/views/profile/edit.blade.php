@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/profile.style.css') }}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Profile</h3>
                        <form>
                            <div class="col-sm-2">
                                <img src="{{ asset('images/user-default.png') }}" id="previewPicture" class="img-responsive img-rounded" alt="profile picture">
                                <input id="picture" accept="image/*" type="file">
                            </div>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label for="email">Email :</label>
                                    <input type="email" disabled class="form-control" id="email" placeholder="Email" value="Falilou17@gmail.com">
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Last Name :</label>
                                    <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Last Name" value="Kébé">
                                </div>
                                <div class="form-group">
                                    <label for="firstname">First Name :</label>
                                    <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First name" value="Falilou Mbacké">
                                </div>
                                <div class="form-group">
                                    <label for="gender">Gender :</label>
                                    <select class="form-control" name="gender">
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="New Password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                                </div>
                                <button type="submit" class="btn btn-success col-sm-offset-1 col-sm-4 pull-right"><i class="fa fa-save"></i> Submit</button>
                                <a href="{{ url('profile') }}" class="btn btn-danger col-sm-3 pull-right"><i class="fa fa-close"></i> Cancel</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(id).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#picture").change(function () {
                readURL(this, '#previewPicture');
            });
        });
    </script>
@endsection