@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/profile.style.css') }}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Profile</h3>
                        <div class="col-sm-2"><img src="{{ asset('images/user-default.png') }}" class="img-responsive img-rounded" alt="profile picture"></div>
                        <div class="col-sm-10">
                            <div class="group">
                                <label>Email :</label>
                                <h4>Falilou17@gmail.com</h4>
                            </div>
                            <div class="group">
                                <label>Last Name :</label>
                                <h4>Kébé</h4>
                            </div>
                            <div class="group">
                                <label>First Name :</label>
                                <h4>Falilou Mbacké</h4>
                            </div>
                            <div class="group">
                                <label>Gender :</label>
                                <h4>Male</h4>
                            </div>
                            <a href="{{ url('profile/edit') }}" class="btn btn-warning col-sm-4 pull-right" type="button">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection