@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/index.style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.full.min.css') }}">
@endsection
@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-7">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1><b>The one-stop shop for train travel</b></h1>
                        <h4>Access fares, schedules and maps of all European train networks and book with the #1
                            worldwide distributor of train tickets and rail passes.</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="tab-header active"><a href="#search" aria-controls="search" role="tab"
                                                                         data-toggle="tab">Find train tickets</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="search">
                        <form class="form-horizontal">
                            <div class="line">
                                <label class="radio-inline">
                                    <input type="radio" id="oneWay" name="inlineRadioOptions" value="One-way" checked> One-way
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" value="Round-trip"> Round-trip
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-group-md">
                                    <input type="text" class="form-control" id="from" autocomplete="off" placeholder="From">
                                    <div class="autoComp"></div>
                                </div>
                                <span id="switch"><img src="{{ asset('images/switch.png') }}" alt=""></span>
                                <div class="col-sm-3 input-group-md">
                                    <input type="text" class="form-control" autocomplete="off" id="to" placeholder="To">
                                    <div class="autoComp"></div>
                                </div>
                                <div class="form-group col-md-3 input-group-md has-feedback">
                                    <input id="passengerInput" readonly="readonly" value="1" type="text" class="form-control modal-input" data-toggle="modal" data-target="#myModal">
                                    <input id="adultInput" value="1" readonly="readonly" name="adultPassenger" type="hidden">
                                    <input id="childInput" value="0" readonly="readonly" name="childPassenger" type="hidden">
                                    <input id="seniorInput" value="0" readonly="readonly" name="seniorPassenger" type="hidden">
                                    <input id="youthInput" value="0" readonly="readonly" name="youthPassenger" type="hidden">
                                    <i class="form-control-feedback glyphicon glyphicon-chevron-down"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-group-md has-feedback">
                                        <input type="text" id="datetimepicker1" class="form-control">
                                        <i class="form-control-feedback glyphicon glyphicon-calendar"></i>
                                </div>
                                <div class="col-sm-3 input-group-md has-feedback">
                                    <input type="text" id="datetimepicker2" class="form-control">
                                    <i class="form-control-feedback glyphicon glyphicon-calendar"></i>
                                </div>
                            </div>
                            <div class="form-group pull-right col-md-3">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-default col-md-12" >Find Tickets > </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" id="myModal" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <label class="col-md-6">Adult</label>
                        <div class="col-md-6">
                            <div class="row">
                                <button type="button" id="addAdult" class="btn btn-default pull-left">+</button>
                                <input type="number" min="0" id="adult" class="col-md-6 input-passenger text-center" value="1">
                                <button type="button" id="minusAdult"  class="btn btn-default">-</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-6">Child</label>
                        <div class="col-md-6">
                            <div class="row">
                                <button type="button" id="addChild" class="btn btn-default pull-left">+</button>
                                <input type="number" min="0" id="child" class="col-md-6 input-passenger text-center" value="0">
                                <button type="button" id="minusChild" class="btn btn-default">-</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-6">Senior</label>
                        <div class="col-md-6">
                            <div class="row">
                                <button type="button" id="addSenior" class="btn btn-default pull-left">+</button>
                                <input type="number" min="0" id="senior" class="col-md-6 input-passenger text-center" value="0">
                                <button type="button" id="minusSenior" class="btn btn-default">-</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-6">Youth</label>
                        <div class="col-md-6">
                            <div class="row">
                                <button type="button" id="addYouth" class="btn btn-default pull-left">+</button>
                                <input type="number" min="0" id="youth" class="col-md-6 input-passenger text-center" value="0">
                                <button type="button" id="minusYouth" class="btn btn-default">-</button>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ok</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript">
        function addOne(id) {
            var value = eval($('#'+id).val())+1;
            $('#'+id).val(value);
            $('#'+id+'Input').val(value);
            passengerNumber('add');
        }
        function minusOne(id) {
            if($('#'+id).val() > 0){
                var value = eval($('#'+id).val())-1;
                $('#'+id).val(value);
                $('#'+id+'Input').val(value);
                passengerNumber('minus');
            }
        }
        function passengerNumber(action) {
            switch(action){
                case 'add' :
                    $('#passengerInput').val(eval($('#passengerInput').val())+1);
                    break;
                case 'minus' :
                    if(eval($('#passengerInput').val()) > 0)
                        $('#passengerInput').val(eval($('#passengerInput').val())-1);
                    break;
            }
        }
        $(document).ready(function () {
            var actualDateTime = moment().format('YYYY/MM/DD HH:mm');

            $('#from').keyup(function () {
                if($('#from').val().length > 1){
                    $.ajax({
                        type: 'GET',
                        url:'https://api.navitia.io/v1/coverage/fr-idf/places?q='+$('#from').val(),
                        dataType: 'json',
                        headers: {
                            Authorization: 'Basic ' + btoa('fd6530c9-96c8-4b54-9e57-90d90b4f2c46')
                        },
                        success: function(data){
                            if(data['places'] != null){
                                $('.autoComp:eq(0)').html("");
                                $('.autoComp:eq(0)').slideDown();
                                data['places'].forEach(function(place) {
                                    $('.autoComp:eq(0)').html($('.autoComp:eq(0)').html() + "<span class='col-sm-12 departureStation'>"+place['name'] + "</span>");
                                });
                            }else
                                $('.autoComp:eq(0)').slideUp();
                        },
                        error: function() {
                            $('.autoComp:eq(0)').slideUp();
                        }
                    });
                }else
                    $('.autoComp:eq(0)').slideUp();
            });
            $('#to').keyup(function () {
                if($('#to').val().length > 1){
                    $.ajax({
                        type: 'GET',
                        url:'https://api.navitia.io/v1/coverage/fr-idf/places?q='+$('#to').val(),
                        dataType: 'json',
                        headers: {
                            Authorization: 'Basic ' + btoa('fd6530c9-96c8-4b54-9e57-90d90b4f2c46')
                        },
                        success: function(data){
                            if(data['places'] != null){
                                console.log(data['places']);
                                $('.autoComp:eq(1)').html("");
                                $('.autoComp:eq(1)').slideDown();
                                data['places'].forEach(function(place) {
                                    $('.autoComp:eq(1)').html($('.autoComp:eq(1)').html() + "<span class='col-sm-12 arrivalStation'>"+place['name'] + "</span>");
                                });
                            }else
                                $('.autoComp:eq(1)').slideUp();
                        },
                        error: function() {
                            $('.autoComp:eq(1)').slideUp();
                        }
                    });
                }else
                    $('.autoComp:eq(1)').slideUp();
            });
            $(document).on('click', '.departureStation',function () {
                $('#from').val($(this).html());
                $('.autoComp:eq(0)').hide();
            });
            $(document).on('click', '.arrivalStation',function () {
                $('#to').val($(this).html());
                $('.autoComp:eq(1)').hide();
            });

            $('#datetimepicker1')
                .datetimepicker({
                    minDate: 0
                })
                .val(actualDateTime)
                .change(function() {
                    var startDate = $('#datetimepicker1').val();
                    $('#datetimepicker2').datetimepicker({
                        minDate: startDate
                    }).val(startDate);
            });

            $('#datetimepicker2').datetimepicker({
                minDate: 0
            }).val(actualDateTime);

            $('#switch').click(function () {
                content = $('#from').val();
                $('#from').val($('#to').val());
                $('#to').val(content);
            });
            $( "input" ).on( "click", function() {
                if($( "input:checked" ).val() == "Round-trip"){
                    $('#datetimepicker2').attr('disabled',false);
                }else{
                    $('#datetimepicker2').attr('disabled',true);
                }
            });
            $("#addAdult").click(function () {
                addOne("adult");
            });
            $("#minusAdult").click(function () {
                minusOne("adult");
            });
            $("#addChild").click(function () {
                addOne("child");
            });
            $("#minusChild").click(function () {
                minusOne("child");
            });
            $("#addSenior").click(function () {
                addOne("senior");
            });
            $("#minusSenior").click(function () {
                minusOne("senior");
            });
            $("#addYouth").click(function () {
                addOne("youth");
            });
            $("#minusYouth").click(function () {
                minusOne("youth");
            });
        });
    </script>
@endsection
