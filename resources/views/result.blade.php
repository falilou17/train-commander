@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/home.style.css') }}">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Journeys</h3>
                    <table class="table table-bordered table-responsive text-center">
                        <th class="text-center">Departure</th>
                        <th class="text-center">Arrival</th>
                        <th class="text-center">From</th>
                        <th class="text-center">To</th>
                        <th class="text-center">Purchase on</th>
                        <th class="text-center">Action</th>
                        <tr>
                            <td>13:41:00</td>
                            <td>13:43:00</td>
                            <td>Gare de Lyon (Paris)</td>
                            <td>Bercy (Paris)</td>
                            <td>23/04/16 13:43:00</td>
                            <td>
                                <button class="btn btn-xs btn-success col-sm-6">reorder</button>
                                <button class="btn btn-xs btn-warning col-sm-4 pull-right">print</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
