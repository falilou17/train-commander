<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = 'profiles';
    protected $fillable = ['userId','lastName','firstName','gender','picture'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
