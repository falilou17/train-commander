<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    //
    protected $table = 'paypals';
    protected $fillable = ['userId'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
