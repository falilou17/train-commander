<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $table = 'reservations';
    protected $fillable = ['departureStation', 'arrivalStation,','departureTime','arrivalTime','userId','status','paypalId','tripId'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
