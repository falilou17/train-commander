<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartialTrip extends Model
{
    //
    protected $table = 'partialtrips';
    protected $fillable = ['nbrPlace'];
}
