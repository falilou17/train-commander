<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    //
    protected $table = 'socials';
    protected $fillable = ['name', 'token', 'userId,','socialId'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
