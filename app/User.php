<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne('App\Models\Profile');
    }

    public function socials(){
        return $this->hasMany('App\Models\Social');
    }

    public function paypals(){
        return $this->hasMany('App\Models\Paypal');
    }

    public function reservations(){
        return $this->hasMany('App\Models\Reservation');
    }

    public function roles(){
        return $this->hasMany('App\Models\Role');
    }
}
