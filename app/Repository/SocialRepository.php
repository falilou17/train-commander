<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 17/04/2016
 * Time: 16:44
 */

namespace App\Repository;


use App\Models\Social;

class SocialRepository
{
    public function all(){
        try{
            $socials = Social::all();
            return ['error'=>false,  'data'=>$socials];
        }catch (Exception $e){
            return ['error'=>true,  'data'=>null];
        }
    }

    public function get($id){
        try{
            $social = Social::findOrFail($id);
            return ['error'=>false, 'data'=>$social];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function store(Request $req){
        try{
            $social = Social::create($req->all());
            return ['error'=>false, 'data'=>$social];
        }catch (Exception $e){
            return  ['error'=>true, 'data'=>null];;
        }
    }

    public function update(Request $req, $id){
        try{
            $social = Social::findOrFail($id);
            $social->fill($req->all());
            $social->save();
            return ['error'=>false, 'data'=>$social];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function destroy($id){
        try{
            $social = Social::findOrFail($id);
            $social->delete();
            return ['error'=>false];
        }catch (Exception $e){
            return ['error'=>true];
        }
    }
}