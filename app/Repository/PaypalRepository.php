<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 17/04/2016
 * Time: 16:44
 */

namespace App\Repository;


use App\Models\Paypal;

class PaypalRepository
{
    public function all(){
        try{
            $paypals = Paypal::all();
            return ['error'=>false,  'data'=>$paypals];
        }catch (Exception $e){
            return ['error'=>true,  'data'=>null];
        }
    }

    public function get($id){
        try{
            $paypal = Paypal::findOrFail($id);
            return ['error'=>false, 'data'=>$paypal];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function store(Request $req){
        try{
            $paypal = Paypal::create($req->all());
            return ['error'=>false, 'data'=>$paypal];
        }catch (Exception $e){
            return  ['error'=>true, 'data'=>null];;
        }
    }

    public function update(Request $req, $id){
        try{
            $paypal =  Paypal::findOrFail($id);
            $paypal->fill($req->all());
            $paypal->save();
            return ['error'=>false, 'data'=>$paypal];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function destroy($id){
        try{
            $paypal = Paypal::findOrFail($id);
            $paypal->delete();
            return ['error'=>false];
        }catch (Exception $e){
            return ['error'=>true];
        }
    }
}