<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 17/04/2016
 * Time: 16:44
 */

namespace App\Repository;


use App\Models\Reservation;

class ReservationRepository
{
    public function all(){
        try{
            $reservations = Reservation::all();
            return ['error'=>false,  'data'=>$reservations];
        }catch (Exception $e){
            return ['error'=>true,  'data'=>null];
        }
    }

    public function get($id){
        try{
            $reservation = Reservation::findOrFail($id);
            return ['error'=>false, 'data'=>$reservation];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function store(Request $req){
        try{

            $reservation = Reservation::create($req->all());
            return ['error'=>false, 'data'=>$reservation];

        }catch (Exception $e){
            return  ['error'=>true, 'data'=>null];;
        }
    }

    public function update(Request $req, $id){

        try{
            $reservation =  Reservation::findOrFail($id);
            $reservation->fill($req->all());
            $reservation->save();

            return ['error'=>false, 'data'=>$reservation];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function destroy($id){
        try{
            $reservation = Reservation::findOrFail($id);
            $reservation->delete();
            return ['error'=>false];
        }catch (Exception $e){
            return ['error'=>true];
        }
    }
}