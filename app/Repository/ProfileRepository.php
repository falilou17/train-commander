<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 17/04/2016
 * Time: 16:44
 */

namespace App\Repository;


use App\Models\Profile;

class ProfileRepository
{
    public function all(){
        try{
            $profiles = Profile::all();
            return ['error'=>false,  'data'=>$profiles];
        }catch (Exception $e){
            return ['error'=>true,  'data'=>null];
        }
    }

    public function get($id){
        try{
            $profile = Profile::findOrFail($id);
            return ['error'=>false, 'data'=>$profile];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function store(Request $req){
        try{
            $profile = Profile::create($req->all());
            return ['error'=>false, 'data'=>$profile];
        }catch (Exception $e){
            return  ['error'=>true, 'data'=>null];;
        }
    }

    public function update(Request $req, $id){
        try{
            $profile =  Profile::findOrFail($id);
            $profile->fill($req->all());
            $profile->save();
            return ['error'=>false, 'data'=>$profile];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function destroy($id){
        try{
            $profile = Profile::findOrFail($id);
            $profile->delete();
            return ['error'=>false];
        }catch (Exception $e){
            return ['error'=>true];
        }
    }
}