<?php
/**
 * Created by PhpStorm.
 * User: faliloumbackekebe
 * Date: 17/04/2016
 * Time: 16:44
 */

namespace App\Repository;


use App\Models\PartialTrip;

class PartialTripRepository
{
    public function all(){
        try{
            $partialTrips = PartialTrip::all();
            return ['error'=>false,  'data'=>$partialTrips];
        }catch (Exception $e){
            return ['error'=>true,  'data'=>null];
        }
    }

    public function get($id){
        try{
            $partialTrip = PartialTrip::findOrFail($id);
            return ['error'=>false, 'data'=>$partialTrip];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function store(Request $req){
        try{
            $partialTrip = PartialTrip::create($req->all());
            return ['error'=>false, 'data'=>$partialTrip];
        }catch (Exception $e){
            return  ['error'=>true, 'data'=>null];;
        }
    }

    public function update(Request $req, $id){
        try{
            $partialTrip =  PartialTrip::findOrFail($id);
            $partialTrip->fill($req->all());
            $partialTrip->save();
            return ['error'=>false, 'data'=>$partialTrip];
        }catch (Exception $e){
            return ['error'=>true, 'data'=>null];
        }
    }

    public function destroy($id){
        try{
            $partialTrip = PartialTrip::findOrFail($id);
            $partialTrip->delete();
            return ['error'=>false];
        }catch (Exception $e){
            return ['error'=>true];
        }
    }
}