<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' =>  'required|min:2',
            'lastName'  =>  'required|min:2',
            'gender'  =>  'required',
            'picture'  =>  'mimes:jpg,jpeg,png',
            'password' => 'string|min:6|confirmed',
            'password_confirmation' => 'string|min:6'
        ];
    }
}
