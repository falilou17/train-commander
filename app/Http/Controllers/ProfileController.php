<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $profile = Auth::user()->profile;
        return view('profile.index'/*, compact('profile')*/);
    }

    /**
     * Show the form to edit specific resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
//        $profile = Auth::user()->profile;
        return view('profile.edit'/*, compact('profile')*/);
    }
}
