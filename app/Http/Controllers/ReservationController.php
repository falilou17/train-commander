<?php

namespace App\Http\Controllers;

use App\Repository\ReservationRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class ReservationController extends Controller
{
    protected $reservationRepository;

    public function __construct(ReservationRepository $reservationRepository)
    {
        $this->reservationRepository = $reservationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $reservations = $this->reservationRepository->all()['data'];
        // return view('', compact('reservations'));
    }
}
